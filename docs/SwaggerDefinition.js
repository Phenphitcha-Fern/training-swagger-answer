const config = require('config');
const swaggerJsdoc = require('swagger-jsdoc');
const swaggerUI = require('swagger-ui-express');

const options = {
    swaggerDefinition: {
        info: {
            description: 'This is document for leave service',
            version: '1.0.0',
            title: 'API Documents'
        },
        host: config.leaveService.host,
        basePath: '/api'
    },
    apis: [
        './docs/LookupDefinition.yml',
        './docs/SummaryDefinition.yml',
        './docs/ResponseDefinition.yml',
        './app/routes/Router.js'
    ]
};

const swaggerOptions = {
    swaggerOptions: {
        defaultModelsExpandDepth: -1
    }
};

const specs = swaggerJsdoc(options);
const docApp = app => {
    app.use(
        '/api-docs',
        swaggerUI.serve,
        swaggerUI.setup(specs, swaggerOptions)
    );
};

module.exports = docApp;
