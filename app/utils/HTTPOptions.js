class HttpOptions {
    constructor(method, contentType) {
        this.option = {
            method: method,
            headers: {
                'Content-Type': contentType || 'application/json'
            }
        };
    }

    addPort(port) {
        this.option.port = port;
        return this;
    }

    addUserID(userID) {
        this.option.headers = {
            ...this.option.headers,
            UserID: userID
        };
        return this;
    }

    addLanguage(language) {
        this.option.headers = {
            ...this.option.headers,
            'Accept-Language': language
        };
        return this;
    }

    addContent(body) {
        try {
            let convertedBody = JSON.stringify(body);

            this.option.body = convertedBody;
            this.option.headers = {
                ...this.option.headers,
                'Content-Length': Buffer.byteLength(convertedBody)
            };
        } catch (error) {
            console.log('convert error:', error);
        }
        return this;
    }

    addUserAgent(userAgent) {
        this.option.headers = {
            ...this.option.headers,
            'User-Agent': userAgent
        };
        return this;
    }

    addHeader(headers) {
        this.option.headers = {
            ...this.option.headers,
            ...headers
        };
        return this;
    }

    addHost(host) {
        this.option.headers.host = host;
        return this;
    }

    addTokenBearer(token) {
        this.option.headers = {
            ...this.option.headers,
            Authorization: 'Bearer ' + token
        };
        return this;
    }

    addAPIKey(apiKey) {
        this.option.headers['x-api-key'] = apiKey;
        return this;
    }

    setTimeout(millisecond = 30000) {
        this.option.timeout = millisecond;
        return this;
    }
}

module.exports = HttpOptions;
