const joi = require('@hapi/joi');
const moment = require('moment');
const _ = require('lodash');
const errorMessage = require('../constants/ErrorMessage');
const { meta } = require('../constants/Constant');
const joiValidator = require('./JoiValidator');

const { PERIOD_SPECIFIC_ID } = require('../constants/Constant');

module.exports.validateSchema = schema => (req, res, next) => {
    let errors = joiValidator(req, schema);

    if (errors) {
        return res.status(400).send({
            meta: meta,
            errors: [
                {
                    source: errors.sources,
                    title: errorMessage.STATUS_400.TITLE_REQUIRE,
                    detail: errors.details
                }
            ]
        });
    }
    next();
};

module.exports.validateCreateLeaveSchema = schema => (req, res, next) => {
    let errors = joiValidator(req, schema);
    errors = errors ? errors.details : [];
    const startTime = moment(req.body.startTime, 'HH:mm');
    const endTime = moment(req.body.endTime, 'HH:mm');

    if (req.body.period == PERIOD_SPECIFIC_ID) {
        if (startTime >= endTime && req.body.endTime !== '00:00') {
            if (errors) {
                errors.push({
                    message: '"endTime" must be larger than to "startTime"'
                });
            } else {
                errors = [
                    {
                        message: '"endTime" must be larger than to "startTime"'
                    }
                ];
            }
        }
        if (req.body.startTime === '') {
            if (errors) {
                errors.push({
                    message: '"startTime" is required'
                });
            } else {
                errors = [
                    {
                        message: '"startTime" is required'
                    }
                ];
            }
        }
        if (req.body.endTime === '') {
            if (errors) {
                errors.push({
                    message: '"endTime" is required'
                });
            } else {
                errors = [
                    {
                        message: '"endTime" is required'
                    }
                ];
            }
        }
    }

    if (errors.length > 0) {
        return res.status(400).send({
            meta: meta,
            errors: [
                {
                    source: errors.sources,
                    title: errorMessage.STATUS_400.TITLE_REQUIRE,
                    detail: errors
                }
            ]
        });
    }
    next();
};
