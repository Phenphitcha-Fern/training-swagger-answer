const express = require('express');
const router = express.Router();
const { meta } = require('../constants/Constant');
const getLut = require('../model/lookup/LookupModel');

router.get('/leavePeriod', async (req, res) => {
    let data = await getLut('leavePeriod', req.language);
    res.status(200).send({ meta: meta, data });
});

router.get('/leaveType', async (req, res) => {
    let data = await getLut('leaveType', req.language);
    res.status(200).send({ meta: meta, data });
});

router.get('/leaveState', async (req, res) => {
    let data = await getLut('leaveState', req.language);
    res.status(200).send({ meta: meta, data });
});

module.exports = router;
