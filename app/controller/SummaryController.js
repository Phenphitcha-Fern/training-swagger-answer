const express = require('express');
const router = express.Router();
const { meta } = require('../constants/Constant');

const summaryMonthModel = require('../model/summary/SummaryMonthModel');
const summaryYearModel = require('../model/summary/SummaryYearModel');
const MiddlewareFormatter = require('../utils/MiddlewareFormatter');
const { getSummaryMonthSchema, getSummaryYearSchema } = require('../schema/SummarySchema');

const joiSummaryMonth = MiddlewareFormatter.validateSchema(getSummaryMonthSchema);
const joiSummaryYear = MiddlewareFormatter.validateSchema(getSummaryYearSchema);

router.get('/month', joiSummaryMonth, async (req, res) => {
    let data = await summaryMonthModel(req.query.uuid, req.query.year, req.query.month, req.language);
    res.status(200).send({ meta: meta, data });
});

router.get('/year', joiSummaryYear, async (req, res) => {
    const language = req.language;
    const { uuid, year, type } = req.query;
    let data = await summaryYearModel({ uuid, year, type, language });
    res.status(200).send({ meta: meta, data });
});

module.exports = router;
