const express = require('express');
const router = express.Router();

const LookupController = require('../controller/LookupController');
const SummaryController = require('../controller/SummaryController');

/**
 * @swagger
 * tags:
 *   name: lookup
 *   description: ข้อมูล Lookup
 */
router.use('/lookup', LookupController);

/**
 * @swagger
 * tags:
 *   name: summary
 *   description: สรุปจำนวนวันลาสะสม
 */
router.use('/summary', SummaryController);

module.exports = router;
